\documentclass [11pt, a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[czech]{babel}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{indentfirst}
\usepackage{url}

\begin{document}


% --- Title page --- %
\pagestyle{empty}

\begin{figure}[!h]
\includegraphics[width=6cm]{img/logo_cvut.pdf}
\end{figure}
\vspace{3 cm}

\begin{center}
{\bf \Large Semestrální projekt MI-PAR 2012/2013}\\[0.5 cm]
{\large Paralelní algoritmus pro řešení problému pokrývání plochy obdélníky}\\[1 cm]
\end{center}

\vfill
\begin{flushright}
{Jan Bělohoubek ({\it belohja4@fel.cvut.cz})\\}
{\footnotesize magisterské studium, FEL ČVUT\\}
{Rostislav Lisový ({\it lisovy@gmail.com})\\}
{\footnotesize doktorské studium, FEL ČVUT\\[0.5 cm]}
{\today \\}
\end{flushright}

\newpage
\pagestyle{plain}

% --- Doc body --- %

\section{Definice problému}
Jsou dána dvě celá čísla $n$ a $m$ představující velikost šachovnice $P$, $Q = \{1,2,3\}$ je množina tří \textit{obdélníkových tvarů} písmene \uv{O} o velikostech 3$\times$3, 2$\times$4 a 1$\times$5. Cílem je nalézt pokrytí šachovnice $P$ danými obdélníky tak, aby zůstalo co nejméně nevyplněných políček a zároveň každý typ obdélníku byl použit alespoň jednou.

\begin{figure}[!ht]
  \begin{center}
    \includegraphics[width=6cm]{img/field.pdf}
  \end{center}
  \caption{Příklad vyplnění pole 5$\times$8.}
  \label{fig:stav_prost}
\end{figure}

\section{Popis sekvenčního algoritmu}
Sekvenční algoritmus řešící výše popsanou úlohu je typu BB-DFS. Triviální dolní mez je rovna $n * m - (9 + 8 + 5)$, tj. každá dlaždička je použita právě jednou. Horní mez je rovna 0, tj. nezbývá žádné nepokryté místo. Algoritmus se zastaví, pokud najde \textit{přípustné} zaplnění šachovnice. Pokud není možné nalézt \textit{přípustné} zaplnění šachovnice, je prohledáván celý stavový prostor.

Prohledávání do hloubky je z důvodů pozdější paralelizace implementováno pomocí \textit{iterativního cyklu} využívajícího vlastní implementaci zásobníku. Jednotlivé \textit{stavy šachovnice} uložené v paměti (na zásobníku) obsahují pouze informaci o \textit{poslední provedené změně} na šachovnici -- nikoliv celou šachovnici. Implementace využívá staticky alokované proměnné pro ukládání informace o rozměrech šachovnice, počtu použití jednotlivých \textit{obdélníkových tvarů}, počtu volných polí v šachovnici a obsazenosti jednotlivých políček šachovnice (v popisu algoritmu je tato struktura označována jako \textit{globální šachovnice}).
Dynamicky alokované struktury jsou struktury popisující jednotlivé \textit{stavy}.

Základní kroky algoritmu jsou:
\begin{itemize}
\item Vlož prvotní (prázdný) stav na zásobník (označený jako OPENED)
\item Dokud není zásobník prázdný, prováděj:
	\begin{itemize}
	\item Zkontroluj míru pokrytí šachovnice a základní podmínku přípustnosti -- rozhodni zda má algoritmus terminovat
	\item Vyjmi stav ze zásobníku
	\item Je-li stav OPENED:
		\begin{itemize}
		\item Označ ho jako CLOSED a vlož ho zpátky na zásobník
		\item Přidej ke \textit{globální šachovnici} tvar, který je obsažen v daném stavu
		\item Proveď expanzi stavu -- pro každé neobsazené políčko se pokus vytvořit stav, který \textit{korektně} vkládá obdélníkový tvar. Tento nově vytvořený stav, označený jako OPENED vlož na zásobník.
		\end{itemize}
	\item Je-li stav CLOSED:
		\begin{itemize}
		\item Jedná se o \textit{back-tracking} -- odeber z \textit{globální šachovnice} tvar, který obsahoval daný stav (a uprav hodnotu čítačů \textit{použití jednotlivých tvarů})
		\end{itemize}
	\end{itemize}
\end{itemize}

\section{Popis paralelního algoritmu a jeho implementace s použitím MPI}
Při implementaci paralelního algoritmu byla využita knihovna MPI usnadňující sdílení dat mezi jednotlivými instancemi nezávislých výpočtů. Základní kroky algoritmu (práce se zásobníkem, expanze stavů, úprava globální šachovnice) se v paralelním algoritmu příliš neliší od sekvenčního. Jednotlivé odlišnosti paralelního algoritmu jsou popsány v následujících podkapitolách.

\subsection{Dělení zásobníku}
Paralelní výpočet je těsně po spuštění ve stavu, kdy jedna instance má parametry výpočtu zadané uživatelem a ostaní jsou zablokovány čekáním na příjmu \textit{informací potřebných k zahájení výpočtu}. Tato první instance vyhodnotí správnost parametrů výpočtu a (v případě, že je možné dále pokračovat) je předá ostatním instancím. Poté spustí algoritmus expanze stavů -- v případě, že zásobník obsahuje stejný nebo větší počet OPENED stavů než je počet instancí, začne jednotlivé stavy vyjímat ze zásobníku a posílat je jednotlivým instancím čekajícím na \textit{práci}.

\subsection{Rozdělování a ukončení práce}
V případě, že některá instance vyčerpá všechny stavy vytvořené expanzí \textit{prvotního stavu}, oznámí tento fakt ostatním instancím. Toto oznámení je označováno jako \textit{posílání peška} (\textit{token passing}) a má za cíl buď získat nová data ke spuštění dalšího výpočtu nebo ukončení celého výpočtu (v případě, že již všechny jiné instance ukončily svoji práci).

Pešek může být buď ve stavu \textit{aktivní} nebo \textit{neaktivní}.
Algoritmus \textit{posílání peška} implementovaný v této práci je následující:

\begin{itemize}
\item V případě, že instance výpočtu nemá žádnou práci, vytvoří a pošle aktivního peška \textit{sousední instanci} -- tj. instanci s identifikátorem o~jedno větší (+ modulo počet instancí)
\item V případě, že některá instance přijme peška
	\begin{itemize}
	\item A zároveň má práci (a pešek je aktivní), dojde k rozdělení zásobníku a odeslání práce instanci, která peška zaslala. Pešek je zahozen
	\item A zároveň nemá práci (a pešek je aktivní nebo neaktivní)
		\begin{itemize}
		\item A zároveň v minulém kroku nepřeposlala žádnou práci -- přepošle peška \textit{sousední instanci}
		\item A zároveň v minulém kroku přeposlala práci -- označí následujících $x$ pešků (kde $x$ je počet instancí) za neaktivní
		\end{itemize}
	\end{itemize}
\item Pokud se původci peška vrátí aktivní pešek, znamená to, že již výpočet končí
\item Pokud se původci peška vrátí neaktivní pešek, tato instance znovu vyšle aktivního peška
\end{itemize}



\section{Naměřené výsledky a vyhodnocení}
Měření byla prováděna při běhu programu na \textit{výpočetním svazku STAR}. Jedná se o 14 jednotek typu Blade propojených sítí InfiniBand a standardním 1Gbit Ethernetem. Topologie tvoří úplný graf, tj. je možná přímá komunikace mezi libovolnými výpočetními jádry.


\subsection{Chovaní algoritmu}

% -- Table 1  - seqv. alg. -- %
\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|c||c|}
      \hline
      Rozměry šachovnice (X $\times$ Y) & čas běhu [s]\\
      \hline
      \hline
      6 $\times$ 5 & 0,15\\
      \hline
      7 $\times$ 6 & 0,11\\
      \hline
      7 $\times$ 10 & 21,92\\
      \hline
      8 $\times$ 9 & 462,92 ($\approx$ 7,7 min)\\
      \hline
      10 $\times$ 11 & 834,13 ($\approx$ 14 min)\\
      \hline
      11 $\times$ 10 & 24,49\\
      \hline
      10 $\times$ 13 & 154,19\\
      \hline
      9 $\times$ 12 & 53,57\\
      \hline
      12 $\times$ 9 & 500,43 ($\approx$ 8 min)\\
      \hline
      11 $\times$ 12 & 3,84\\
      \hline
      12 $\times$ 11 & 1932,20\\
      \hline
      12 $\times$ 12 & 3258,43\\
      \hline      
    \end{tabular}
  \end{center}
  \caption{Čas běhu sekvenčního algoritmu na jednom výpočetním jádru svazku STAR pro různé rozměry šachovnice.}
  \label{tab:seqv_times}
\end{table}

Z tabulky \ref{tab:seqv_times} je dobře patrné, že čas běhu sekvenčního algoritmu nezávisí pouze na velikosti šachovnice, ale také na jejím rozvržení -- čas běhu algoritmu se výrazně liší například pro šachovnici 5 $\times$ 12 a 12 $\times$ 9. To je dáno tím, že algoritmus je ukončen, když nalezne správné řešení. Je jasné, že pro každé rozvržení šachovnice může být správné řešení nalezeno po jiném počtu kroků. Proto je čas běhu algoritmu pro různá rozvržení šachovnice tak rozdílný.

\newpage
\subsection{Závislost doby běhu na počtu jader svazku STAR}

% -- Table 1  - times 8 x 9 -- %
\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 1 & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Čas běhu -- měření 1 [s] & 462,92 & 1,44 & 1,48 & 1,49 & 1,74 & 2,15\\
      \hline
      Čas běhu -- měření 2 [s] & 460,41 & 1,43 & 1,51 & 1,49 & 1,73 & 2,70\\
      \hline
      Čas běhu -- měření 3 [s] & 469,41 & 1,42 & 1,51 & 1,49 & 1,73 & 2,70\\
      \hline
      Průměrný čas běhu [s] & 464,25 & 1,43 & 1,50 & 1,49 & 1,73 & 2,52\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Čas běhu algoritmu na různém počtu výpočetních jader svazku STAR pro šachovnici 8 $\times$ 9.}
  \label{tab:times8x9}
\end{table}


% -- Table 1  - times 10 x 11 -- %
\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 1 & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Čas běhu -- měření 1 [s] & 829,97 & 38,38 & 24,37 & 16,54 & 0,68 & 0,92\\
      \hline
      Čas běhu -- měření 2 [s] & 833,31 & 38,57 & 24,54 & 16,58 & 0,67 & 0,84\\
      \hline
      Čas běhu -- měření 3 [s] & 834,13 & 38,78 & 24,44 & 16,45 & 0,51 & 0,83\\
      \hline
      Průměrný čas běhu [s] & 832,47 & 38,58 & 24,45 & 16,52 & 0,62 & 0,86\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Čas běhu algoritmu na různém počtu výpočetních jader svazku STAR pro šachovnici 10 $\times$ 11.}
  \label{tab:times10x11}
\end{table}

% -- Table 1  - times 12 x 9 -- %
\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 1 & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Čas běhu -- měření 1 [s] & 500,69 & 1,58 & 1,63 & 1,60 & 1,94 & 3,14\\
      \hline
      Čas běhu -- měření 2 [s] & 506,26 & 1,54 & 1,61 & 1,62 & 1,95 & 2,63\\
      \hline
      Čas běhu -- měření 3 [s] & 500,43 & 1,54 & 1,64 & 1,59 & 1,91 & 2,52\\
      \hline
      Průměrný čas běhu [s] & 502,46 & 1,55 & 1,63 & 1,60 & 1,93 & 2,76\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Čas běhu algoritmu na různém počtu výpočetních jader svazku STAR pro šachovnici 12 $\times$ 9.}
  \label{tab:times12x9}
\end{table}

\newpage
\subsection{Zrychlení}

Zrychlení se vypočte podle vztahu:
$$S(n,p) = \frac{SU(n)}{T(n,p)},$$
kde $SU(n)$ je doba běhu algoritmu na jednom jádře svazku STAR a $T(n,p)$ je čas běhu na $p$ výpočetních jádrech.

\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 1 & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Zrychlení -- 8 $\times$ 9 & 1 & 324,65 & 309,5 & 311,58 & 268,35 & 184,23 \\
      \hline
      Zrychlení -- 10 $\times$ 11 & 1 & 21,58 & 34,05 & 50,39 & 1342,69 & 967,99 \\
      \hline
      Zrychlení -- 12 $\times$ 9 & 1 & 324,17 & 308,26 & 314,04 & 260,34 & 182,05 \\
      \hline
    \end{tabular}
  \end{center}
  \caption{Tabulka zrychlení.}
  \label{tab:zrychl}
\end{table}

\begin{figure}[!ht]
  \begin{center}
    \includegraphics[width=12cm]{img/graf8x9.jpg}
  \end{center}
  \caption{Graf zrychlení pro pole 8 $\times$ 9.}
  \label{fig:graf8x9}
\end{figure}

\begin{figure}[!ht]
  \begin{center}
    \includegraphics[width=12cm]{img/graf10x11.jpg}
  \end{center}
  \caption{Graf zrychlení pro pole 10 $\times$ 11.}
  \label{fig:graf10x11}
\end{figure}

\begin{figure}[!ht]
  \begin{center}
    \includegraphics[width=12cm]{img/graf12x9.jpg}
  \end{center}
  \caption{Graf zrychlení pro pole 12 $\times$ 9.}
  \label{fig:graf12x9}
\end{figure}

\newpage
\subsection{Efektivnost}
Efektivnost jsme určili z následujícího vztahu:
$$E(n,p) = \frac{S(n,p)}{p},$$
kde $S(n,p)$ je zrychlení a $p$ počet výpočetních jader.

\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 1 & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Efektivnost -- 8 $\times$ 9 & 1 & 162,33 & 77,38 & 38,95 & 16,77 & 7,68\\
      \hline
      Efektivnost -- 10 $\times$ 11 & 1 & 10,79 & 8,51 & 6,30 & 83,92 & 40,33\\
      \hline
      Efektivnost -- 12 $\times$ 9 & 1 & 162,09 & 77,07 & 39,26 & 16,27 & 7,59\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Tabulka efektivnosti.}
  \label{tab:efekt}
\end{table}

\newpage
\subsection{Srovnání komunikační režie stejných instancí nad Ethernetem a Infinibandem}

Předpoklad, že Infiniband poskytne rychlejší propojení se nenaplnil, možným vysvětlením je větší zatížení Infiniband spojů v době měření, oproti tomu provoz na Ethernetu byl minimální.
Tabulky \ref{tab:times10x11_infiniband} a \ref{tab:times10x11_ethernet} ukazují dobu běhu algoritmu v závislosti na použitém přenosovém médiu. Z tabulek je vidět, že komunikační režie Infinibandu je vyšší.

% -- Table 1  - times 10 x 11 - infiniband -- %
\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Čas běhu -- měření 1 [s] & 38,38 & 24,37 & 16,54 & 0,68 & 0,92\\
      \hline
      Čas běhu -- měření 2 [s] & 38,57 & 24,54 & 16,58 & 0,67 & 0,84\\
      \hline
      Čas běhu -- měření 3 [s] & 38,78 & 24,44 & 16,45 & 0,51 & 0,83\\
      \hline
      Průměrný čas běhu [s] & 38,58 & 24,45 & 16,52 & 0,62 & 0,86\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Čas běhu algoritmu na různém počtu výpočetních jader svazku STAR pro šachovnici 10 $\times$ 11 na Infinibandu.}
  \label{tab:times10x11_infiniband}
\end{table}

% -- Table 1  - times 10 x 11 - ethernet -- %
\begin{table}[!ht]\catcode`\-=12
  \begin{center}
    \begin{tabular}{|l||c|c|c|c|c|}
      \hline
      Počet výpočetních jader & 2 & 4 & 8 & 16 & 24\\
      \hline
      \hline
      Čas běhu -- měření 1 [s] & 29,17 & 16,19 & 12,34 & 0,56 & 0,73\\
      \hline
      Čas běhu -- měření 2 [s] & 26,52 & 16,47 & 11,19 & 0,48 & 0,71\\
      \hline
      Čas běhu -- měření 3 [s] & 26,47 & 16,23 & 11,49 & 0,47 & 0,70\\
      \hline
      Průměrný čas běhu [s]  &  27,39 & 16,30 & 11,67 & 0,50 & 0,71\\
      \hline
    \end{tabular}
  \end{center}
  \caption{Čas běhu algoritmu na různém počtu výpočetních jader svazku STAR pro šachovnici 10 $\times$ 11 na Ethernetu.}
  \label{tab:times10x11_ethernet}
\end{table}

\newpage
\section{Závěr}
Ze způsobu práce algoritmu a zvýše uvedených naměřených dat je zřejmé, že pro menší instance, jejichž výpočet trvá na jednom jádře svazku STAR asi 8 minut je řešení paralelně nalezeno velice rychle, téměř okamžitě po prvním dělení zásobníku (dělení je tak \uv{šikovné}, že řešení se po rozdělení zásobníku nachází velmi blízko počátku stavového prostoru jednoho z~výpočetních jader). Tím pádem dochází při zvyšování počtu výpočetních jader k poklesu efektivnosti -- protože přidání dalšího jádra nepřináší žádné zrychlení nalezení řešení, pouze dojde ke zvýšení komunikační režie, která prodlouží čas výpočtu. Pro větší instance nastává obdobná situace také, ale až po několikanásobném rozdělení zásobníku -- negativní vliv se tak projeví až po přidání mnohem většího počtu procesorů, do té doby ovšem dochází k žádoucímu zlepšování.
\par
Jak je vidět z tabulek i z grafů, pro všechna pole je, alespoň pro některý počet výpočetních jader, dosahováno superlineárního zrychlení. To lze vysvětlit tím, že zásobník je půlen \uv{u dna} a správná řešení se nalézají velmi \uv{blízko} místa dělení zásobníku.

\newpage
\begin{thebibliography}{9}
\bibitem{web} Stránky předmětu MI-PAR dostupné na URL \url{https://edux.fit.cvut.cz/courses/MI-PAR}, verze z 1. 11. 2012
\bibitem{mpi} Manuálové stránky MPI API pro jazyk C
\end{thebibliography}


\appendix

 
\end{document}

