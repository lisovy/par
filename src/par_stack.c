#include <stdio.h>
#include "par_stack.h"
#include "par_pr_dbg.h"

int par_stack_init(struct par_stack *stack)
{
	if (!stack)
		return PAR_STACK_ERR_NOSTACK;

	stack->sp = &stack->first_item;
	stack->first_item.parent = NULL;

	return PAR_STACK_ERR_OK;
}

/* Removes the item from the top of the stack, writing the pointer to the item to @item */
int par_stack_pop(struct par_stack *stack, struct par_stack_item **item)
{
	pr_dbg("\npop()\n");

	if (!stack)
		return PAR_STACK_ERR_NOSTACK;

	if (stack->sp == &stack->first_item) {
		*item = NULL;
		return PAR_STACK_ERR_NOITEM;
	}

	*item = stack->sp;
	stack->sp = stack->sp->parent;

	return PAR_STACK_ERR_OK;
}

/* Store the item on the top of the stack */
int par_stack_push(struct par_stack *stack, struct par_stack_item *item)
{
	//print("push() sp = %p\n", stack->sp);
	pr_dbg("\npush()\n");

	if (!stack)
		return PAR_STACK_ERR_NOSTACK;

	if (!item)
		return PAR_STACK_ERR_NOITEM;

	item->parent = stack->sp;
	stack->sp = item;

	return PAR_STACK_ERR_OK;
}

/* Writes to @predecessor the predecessor (the next item in direction
   from top to bottom -- i.e. "older item") of @item.
   This operation does not remove anything from the stack */
int par_stack_get_predecessor(struct par_stack *stack, const struct par_stack_item *item,
			      struct par_stack_item **predecessor)
{
	pr_dbg("\nget_predecessor()\n");

	if (!stack)
		return PAR_STACK_ERR_NOSTACK;

	*predecessor = item->parent;
	if (*predecessor == &stack->first_item) {
		*predecessor = NULL;
		return PAR_STACK_ERR_NOITEM;
	}

	return PAR_STACK_ERR_OK;
}

/* Removes the predecessor (the next item in direction from top to bottom --
   i.e. "older item") of @item from the stack
   Example: To remove item X, we need to pass pointer to the successor of X */
int par_stack_remove_predecessor(struct par_stack *stack,
				 struct par_stack_item *item)
{
	struct par_stack_item *tmp_item;

	pr_dbg("\nremove_predecessor()\n");

	if (!stack)
		return PAR_STACK_ERR_NOSTACK;

	if (item->parent == &stack->first_item) {
		return PAR_STACK_ERR_NOITEM;
	}

	tmp_item = item->parent;
	item->parent = tmp_item->parent;
	tmp_item->parent = NULL;

	return PAR_STACK_ERR_OK;
}

/* Returns pointer to the top item on the stack -- without modifying the stack */
int par_stack_get_top(struct par_stack *stack, struct par_stack_item **item)
{
	pr_dbg("\nget_top()\n");

	if (!stack)
		return PAR_STACK_ERR_NOSTACK;

	if (stack->sp == &stack->first_item) {
		*item = NULL;
		return PAR_STACK_ERR_NOITEM;
	}

	*item = stack->sp;

	return PAR_STACK_ERR_OK;
}
