/*
 * Semestral work for the course "Parallel algorithms and systems" taught
 *   at Czech Technical University in Prague, Faculty of Informatics
 *
 * This program solves the problem of filling arbitrary rectangle
 *   with rectangles of defined sizes while minimizing the unfilled space
 *
 * Copyright (c) 2012 Rostislav Lisovy (lisovy@gmail.com)
 * Copyright (c) 2012 Jan Belohoubek (belohja4@fel.cvut.cz)
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. * See the GNU General Public License for
 * more details.
 */

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <unistd.h> // sleep()

#include "par_stack.h"
#include "par_sem.h"
#include "par_mpi.h"

#define DEBUG_PARSEM 1 /* Enable printing of debuging info */
#undef DEBUG_PARSEM
#define DEBUG_PARSEM_MPI
#undef DEBUG_PARSEM_MPI
#include "par_pr_dbg.h"

#define true 	1
#define false 	0

/******************************************************************************
 * Used terminology: FIELD is place which we are trying to fill with RECTANGLES
 *                   it consists of TILES
 *
 *                   FIELD is sometimes referenced as Global Field or GF
 ******************************************************************************/

/* Global field parameters */
int par_field_width, par_field_height, par_field_size;

struct par_stack stack;

/* Each unsigned char is one tile; Each free tile is set to 0;
   Tile containing a rectangle has value set to particular
   rectangle type (enum par_type)
*/
unsigned char **global_field;
unsigned char **global_field_best; /* Best known solution*/
unsigned char **global_field_tmp;  /* Used when reconstructing some unexpanded
				      state to be sent to some slave */

unsigned int global_field_filled_tiles;

/* Counters containing info about types of rectangles used --
   this is useful for deciding if the state is */
unsigned int global_field_used_rects_types[PAR_RECT_POSTLAST];
unsigned int global_field_used_rects_types_tmp[PAR_RECT_POSTLAST];

int best_state_filled;

/*** MPI **********************************************************************
 * Used terminology: CLOUD is set of all MPI processes
 *                   ROOT is the first MPI process
 *                   SLAVE is non-ROOT MPI process
 *                   NODE is an arbitrary MPI process
 ******************************************************************************/

/* MPI process number */
static int parmpi_my_id;

/* Number of MPI processes */
static int parmpi_instances_cnt;

/* MPI successor process number */
static int parmpi_my_successor;

/* MPI status to distribute into cloud */
static int parmpi_cloud_status;

/* Interval to check for new messages */
static int parmpi_check_inbox_cnt;

/* Stack status - empty == true */
static int parmpi_stack_empty;

/* Buffer for MPI messages -- Used only in the initial configuration phase --
   used for sending Status, GF width, GF height */
static unsigned char parmpi_initconf_buffer[PARMPI_INITCONF_BUFFER_LEN];
/* Dynamically allocated buffer used for receiving MPI messages containing
   GF + particular state to start from */
static unsigned char *parmpi_msg_buffer;
static int mpi_statemsg_size;

int void_buff; /* Buffer used for sending 0-length MPI messages */

double parmpi_starttime;
double parmpi_endtime;
/******************************************************************************/

static inline void pr_dbg_mpi_field(unsigned char **gf)
{
#ifdef DEBUG_PARSEM_MPI
	for (int i = 0; i < par_field_height; i++) {
		for (int j = 0; j < par_field_width; j++) {
			printf("%d ", gf[j][i]);
		}
		printf("\n");
	}
#endif
}

static inline void pr_dbg_field(unsigned char **gf)
{
#ifdef DEBUG_PARSEM
	for (int i = 0; i < par_field_height; i++) {
		for (int j = 0; j < par_field_width; j++) {
			printf("%d ", gf[j][i]);
		}
		printf("\n");
	}
#endif
}

static inline void pr_dbg_state(struct par_state *state, int flags)
{
#ifdef DEBUG_PARSEM
	printf("  state_id     = %i\n", state->id);
	printf("  filled_tiles = %i\n", state->filled_tiles);
	printf("  rect_type    = %i\n", state->rect_type);
	printf("  flags        = %i\n", state->flags);
	//printf("    global_field_used_rects_types = %i\n",
	//	global_field_used_rects_types[PAR_RECT_3_3]);

	if (flags & 1)
		pr_dbg_field(global_field);

	printf("\n");
#endif
}

/******************************************************************************/
void par_gf_cpy(unsigned char **dest, unsigned char **src)
{
	for (int i = 0; i < par_field_width; i++) {
		for (int j = 0; j < par_field_height; j++) {
			dest[i][j] = src[i][j];
		}
	}

}

void par_field_print(unsigned char **gf)
{
	for (int i = 0; i < par_field_width; i++) {
		for (int j = 0; j < par_field_height; j++) {
			//printf("%d ", gf[i][j]);
			if (gf[i][j] != 0){
				printf("%c ", gf[i][j]);
			} else {
				printf("- ");
			}
		}
		printf("\n");
	}
}

int par_field_remove_rect(unsigned char **gf, unsigned int x, unsigned int y,
			  enum par_rect rect_type)
{
	int x_lim = x + par_rect_width[rect_type] - 1;
	int y_lim = y + par_rect_height[rect_type] - 1;

	/* We trust the caller -- so we won't check if the
	   rect is outside the field boundaries */

	for (int i = x; i <= x_lim; i++) {
		for (int j = y; j <= y_lim; j++) {
			gf[i][j] = 0;
		}
	}

	return true;
}

/*
  returns true if rectangle is placed
  returns false if it is not possible to place the rectangle
*/
int par_field_place_rect(unsigned char **gf, unsigned int x, unsigned int y,
			 enum par_rect rect_type)
{
	static char rect_char = 'A';
	int collision = 0;
	int x_lim = x + par_rect_width[rect_type] - 1;
	int y_lim = y + par_rect_height[rect_type] - 1;

	/* Will not fit into field */
	if (x_lim > par_field_width - 1)
		return false;
	if (y_lim > par_field_height - 1)
		return false;

	/* Check if tiles necessary for placing the rectangle are free (i.e. set to 0) */
	for (int i = x; i <= x_lim; i++) {
		for (int j = y; j <= y_lim; j++) {
			collision += gf[i][j];
		}
	}

	if (collision)
		return false;

	/* Place the rectangle */
	for (int i = x; i <= x_lim; i++) {
		for (int j = y; j <= y_lim; j++) {
			gf[i][j] = rect_char;
		}
	}

	if (rect_char == 'Z') {
		rect_char = 'A';
	} else {
		rect_char = rect_char + 1;
	}

	return true;
}

void par_print_usage(char *argv[])
{
	printf("Usage:\n");
	printf("\t%s FIELD_WIDTH FIELD_HEIGHT\n\n", argv[0]);
	printf("Where FIELD_WIDTH, FIELD_HEIGHT is an integer defining filed size\n\n");
}

int par_parse_args(int argc, char *argv[])
{
	errno = 0;
	/* Parse commandline arguments */
	if (argc != 3) {
		par_print_usage(argv);
		return false;
	} else {
		char *endptr;

		par_field_width = strtol(argv[1], &endptr, 10);
		if ((char *)argv[1] == endptr) {
			par_print_usage(argv);
			return false;
		}
		if (errno != 0) {
			par_print_usage(argv);
			//perror("strtol()");
			return false;
		}

		par_field_height = strtol(argv[2], &endptr, 10);
		if ((char *)argv[2] == endptr) {
			par_print_usage(argv);
			return false;
		}
		if (errno != 0) {
			par_print_usage(argv);
			//perror("strtol()");
			return false;
		}
		par_field_size = par_field_height * par_field_width;
	}

	return true;
}

int par_gf_alloc(unsigned char ***gf)
{
	*gf = malloc(sizeof(unsigned char *) * par_field_width);
	if (!*gf) {
		printf("Cannot allocate enough memory!\n");
		return false;
	}

	for (int i = 0; i < par_field_width; i++) {
		(*gf)[i] = malloc(sizeof(unsigned char) * par_field_height);
		if (!(*gf)[i]) {
			/* free(*gf); */
			printf("Cannot allocate enough memory!\n");
			return false;
		}
	}
	return true;
}

int par_gf_dealloc(unsigned char **gf)
{
	for (int i = 0; i < par_field_width; i++) {
		free(gf[i]);
	}
	free(gf);
	return true;
}

void par_gf_init(unsigned char **gf)
{
	for (int i = 0; i < par_field_width; i++) {
		for (int j = 0; j < par_field_height; j++) {
			gf[i][j] = 0;
		}
	}
}

/* This function searches for deepest (as seen in stack) Opened state.
   It deletes this state from stack and modifies the @global_field_tmp to
   correspond to the state */
int par_get_oldest_opened_state(struct par_stack_item **ret_item)
{
	int ret;
	struct par_stack_item *tmp_item;
	struct par_stack_item *tmp_item_scsr = NULL;
	struct par_stack_item *deepest_opened = NULL;
	struct par_stack_item *deepest_opened_scsr = NULL;
	struct par_state *tmp_state;

	/* Awkwardly traversing through the stack (from top to bottom);

	   Stacks internal structure is single linked-list;
	   If we want to delete an item, we need the item which
	   is pointing to it -- thus we need two pointers for *item*

	   The outcome of the traversal should be the pointer to the
	   *deepest OPENED item* -- which we need to remove from the
	   stack.
	   We have to be aware of the fact, that there could be *CLOSED
	   items* on the bottom of the stack -- that's the reasont to use
	   the third pointer.

	   Terminology:

	   Stack       Predecessor            Successor    Stack
	   bottom       of C                   of C         top
				   *Item C*
	   /-----\     /-----\     /-----\     /-----\     /-----\
	   | A   |     | B   |     | C   |     | D   |     | E   |
	   |     | <-- |     | <-- |     | <-- |     | <-- |     |
	   \-----/     \-----/     \-----/     \-----/     \-----/

	   |------------>  Stack is rising in time |------>

	   D is Successor of C; C is Predecessor of D
	   To remove *C* we need pointer to its Successor --
	   either we say we are removing C (using its Successor),
	   or "we are removing Ds Predecessor".
	*/
	ret = par_stack_get_top(&stack, &tmp_item);
	assert(ret == 0);

	/* We have to go through the whole stack,
	   even if there are CLOSED items on the bottom */
	while (1) {
		tmp_state = container_of(tmp_item,
			struct par_state, stack_item);

		if (tmp_state->flags == PAR_STATE_OPENED) {
			deepest_opened = tmp_item;
			deepest_opened_scsr = tmp_item_scsr;
		}

		tmp_item_scsr = tmp_item;
		ret = par_stack_get_predecessor(&stack, tmp_item,
			&tmp_item);
		if (ret == PAR_STACK_ERR_NOITEM)
			break;
	}
	*ret_item = deepest_opened;
	if (deepest_opened == NULL)
		return -1;

	/* Right now we should have the *Deepest opened state*.
	   We have to traverse through the stack the second time --
	   from the top to the *Deepest opened state* and restore the states
	   GF into @global_field_tmp */
	ret = par_stack_get_top(&stack, &tmp_item);
	assert(ret == 0);
	par_gf_cpy(global_field_tmp, global_field);
	memcpy(global_field_used_rects_types_tmp,
		global_field_used_rects_types,
		sizeof(global_field_used_rects_types));

	while (1) {
		if (tmp_item == deepest_opened)
			break;

		tmp_state = container_of(tmp_item,
			struct par_state, stack_item);

		if (tmp_state->flags == PAR_STATE_CLOSED) {
			par_field_remove_rect(global_field_tmp, tmp_state->x,
				tmp_state->y, tmp_state->rect_type);
			global_field_used_rects_types_tmp[tmp_state->rect_type]--;
		}

		tmp_item_scsr = tmp_item;
		ret = par_stack_get_predecessor(&stack, tmp_item_scsr,
			&tmp_item);
	}

	/* Actually remove the item from the stack */
	/* We didn't move from the top */
	if (deepest_opened_scsr == NULL) {
		ret = par_stack_pop(&stack, &tmp_item);
		assert(ret == 0);
	} else {
		ret = par_stack_remove_predecessor(&stack,
			deepest_opened_scsr);
		assert(ret == 0);
	}

	return 0;
}

void parmpi_notify_no_work(void)
{
	int node;

	for (node = 0; node < parmpi_instances_cnt; node++) {
		if (node == parmpi_my_id)
			continue;

		MPI_Send(&void_buff, 1, MPI_INT,
			node, PARMPI_TAG_NO_WORK, MPI_COMM_WORLD);
	}
}

void parmpi_notify_error(void)
{
	int node;

	for (node = 0; node < parmpi_instances_cnt; node++) {
		if (node == parmpi_my_id)
			continue;

		MPI_Send(&void_buff, 1, MPI_INT,
			node, PARMPI_TAG_ERROR, MPI_COMM_WORLD);
	}
}

int parmpi_send_work(struct par_stack_item *oldest_opened, int dest, int tag)
{
	struct par_state *tmp_state;
	int offs = 0;

	tmp_state = container_of(oldest_opened,
		struct par_state, stack_item);
	pr_dbg_mpi("Sending some work\n");
	/* MPI Message format:
	   State description:
		INT: X
		INT: Y
		INT: Rect type
		INT: Filled tiles
	   Global field:
		PACKED: global_field_used_rects_types
		PACKED: Global field
	*/
	MPI_Pack(&tmp_state->x, 1,  MPI_INT, parmpi_msg_buffer,
		mpi_statemsg_size, &offs, MPI_COMM_WORLD);
	MPI_Pack(&tmp_state->y, 1,  MPI_INT, parmpi_msg_buffer,
		mpi_statemsg_size, &offs, MPI_COMM_WORLD);
	MPI_Pack(&tmp_state->rect_type, 1,  MPI_INT,
		parmpi_msg_buffer, mpi_statemsg_size, &offs,
		MPI_COMM_WORLD);
	MPI_Pack(&tmp_state->filled_tiles, 1, MPI_INT,
		parmpi_msg_buffer, mpi_statemsg_size, &offs,
		MPI_COMM_WORLD);
	MPI_Pack(&global_field_used_rects_types_tmp, PAR_RECT_POSTLAST, MPI_INT,
		parmpi_msg_buffer, mpi_statemsg_size, &offs,
		MPI_COMM_WORLD);
	for (int i = 0; i < par_field_width; i++) {
		MPI_Pack(global_field_tmp[i], par_field_height,
			MPI_CHAR, parmpi_msg_buffer,
			mpi_statemsg_size, &offs, MPI_COMM_WORLD);
	}

	MPI_Send(parmpi_msg_buffer, offs, MPI_PACKED,
		dest, tag, MPI_COMM_WORLD);

	return 0;
}

int parmi_receive_work_and_push_state(int tag)
{
	struct par_state *first_state;
	MPI_Status status;
	int offs = 0;
	int ret;

#if 0
	/* Create first fake closed state */
	first_state = malloc(sizeof(struct par_state));
	first_state->x = 0;
	first_state->y = 0;
	first_state->rect_type = PAR_RECT_NONE;
	first_state->filled_tiles = 0;
	first_state->flags = PAR_STATE_CLOSED;
	ret = par_stack_push(&stack, &first_state->stack_item);
	assert(ret == 0);
#endif

	first_state = malloc(sizeof(struct par_state));
	/* MPI TokenReply format:
	  State description:
		INT: X
		INT: Y
		INT: Rect type
		INT: Filled tiles
	  Global field:
		PACKED: global_field_used_rects_types
		PACKED: Global field
	*/
	MPI_Recv(parmpi_msg_buffer, mpi_statemsg_size, MPI_PACKED,
		MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
	pr_dbg_mpi("Received some work\n");
	first_state = malloc(sizeof(struct par_state));

	MPI_Unpack(parmpi_msg_buffer, mpi_statemsg_size, &offs,
		&first_state->x, 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(parmpi_msg_buffer, mpi_statemsg_size, &offs,
		&first_state->y, 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(parmpi_msg_buffer, mpi_statemsg_size, &offs,
		&first_state->rect_type, 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(parmpi_msg_buffer, mpi_statemsg_size, &offs,
		&first_state->filled_tiles, 1, MPI_INT, MPI_COMM_WORLD);
	MPI_Unpack(parmpi_msg_buffer, mpi_statemsg_size, &offs,
		&global_field_used_rects_types, PAR_RECT_POSTLAST, MPI_INT,
		MPI_COMM_WORLD);
	for (int i = 0; i < par_field_width; i++) {
		MPI_Unpack(parmpi_msg_buffer, mpi_statemsg_size, &offs,
			global_field[i], par_field_height, MPI_CHAR,
			MPI_COMM_WORLD);
	}

	first_state->flags = PAR_STATE_OPENED;
	ret = par_stack_push(&stack, &first_state->stack_item);
	assert(ret == 0);
	parmpi_stack_empty = false;

	return ret;
}

int parmpi_root_send_parsed_arguments(int argc, char *argv[])
{
	int offs = 0;

	if (par_parse_args(argc, argv)) {
		parmpi_cloud_status = PARMPI_OK;
	} else {
		parmpi_cloud_status = PARMPI_ERROR;
	}

	/* send parsed arguments to all SLAVES -- send bcast */
	/* MPI message format:
		INT: Status
		INT: GF width
		INT: GF height
	*/
	MPI_Pack(&parmpi_cloud_status, 1,  MPI_INT, parmpi_initconf_buffer,
		PARMPI_INITCONF_BUFFER_LEN, &offs, MPI_COMM_WORLD);
	MPI_Pack(&par_field_width, 1,  MPI_INT, parmpi_initconf_buffer,
		PARMPI_INITCONF_BUFFER_LEN, &offs, MPI_COMM_WORLD);
	MPI_Pack(&par_field_height, 1,  MPI_INT, parmpi_initconf_buffer,
		PARMPI_INITCONF_BUFFER_LEN, &offs, MPI_COMM_WORLD);

	MPI_Bcast(parmpi_initconf_buffer, offs, MPI_PACKED, PARMPI_ROOT, MPI_COMM_WORLD);
	pr_dbg_mpi("Sent: %d x %d\n", par_field_width, par_field_height);

	if (parmpi_cloud_status == PARMPI_ERROR)
		return -1;

	return 0;
}

int parmpi_slaves_receive_parsed_arguments(void)
{
	int offs = 0;

	MPI_Bcast(parmpi_initconf_buffer, 3 * sizeof(int),
		MPI_PACKED, PARMPI_ROOT, MPI_COMM_WORLD);

	MPI_Unpack(parmpi_initconf_buffer, PARMPI_INITCONF_BUFFER_LEN, &offs,
		 &parmpi_cloud_status, 1, MPI_INT, MPI_COMM_WORLD);

	if (parmpi_cloud_status == PARMPI_OK) {
		MPI_Unpack(parmpi_initconf_buffer, PARMPI_INITCONF_BUFFER_LEN, &offs,
			&par_field_width, 1, MPI_INT, MPI_COMM_WORLD);
		MPI_Unpack(parmpi_initconf_buffer, PARMPI_INITCONF_BUFFER_LEN, &offs,
			&par_field_height, 1, MPI_INT, MPI_COMM_WORLD);

		par_field_size = par_field_height * par_field_width;
		pr_dbg_mpi("Received: %d x %d\n", par_field_width, par_field_height);

	} else if (parmpi_cloud_status == PARMPI_ERROR) {
		return -1;
	}

	return 0;
}

int parmpi_check_gf_size(void)
{
	int ret;

	if (par_field_size < PAR_FIELD_MIN) {
		/* GF smaller than PAR_FIELD_MIN won't be able to fullfil the
		   condition to place each rectangle at least once */
		printf("Requested area is too small.\n");
		return PARMPI_ERROR;
	} else {
		ret = par_gf_alloc(&global_field);
		if (!ret)
			return PARMPI_ERROR;

		ret = par_gf_alloc(&global_field_best);
		if (!ret)
			return PARMPI_ERROR;

		ret = par_gf_alloc(&global_field_tmp);
		if (!ret)
			return PARMPI_ERROR;
	}

	return PARMPI_OK;
}

void parmpi_clean_inbox(void)
{
	int flag;
	MPI_Status status;

	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
	while (flag) {
		MPI_Recv(parmpi_msg_buffer, mpi_statemsg_size, MPI_PACKED,
			MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
	}
}

/*
== Token passing algorithm description ==
In case that some Node does not have any work to do (receiving of first
stage + GF does not count to this case), it sends GETWORK_TOKEN to its
successor.
Successor of Node with ID = x is "(x + 1) % parmpi_instances_cnt".

GETWORK_TOKEN may exist in different states:
* GETWORK_TOKEN -- active request for work
* PARMPI_TAG_GETWORK_TOKEN_INVALID -- invalidated token

The GF + state are sent to the Node who requesteds some work in message with
tag NEW_WORK.

Life of GETWORK_TOKEN as seen from some Node which has some work:
* Node receives active token
* It sends some work to Node whose ID is read from GETWORK_TOKEN message
* Received token does not exist anymore

Life of GETWORK_TOKEN as seen from some Node which does not have any work:
* Node receives active token
* In case it didn't lately send some work to somebody else, it
    - sends this token to its successor
* In case it did lately send some work to somebody else, it
    - will Invalidate next "parmpi_instances_cnt - 1" tokens

Life of GETWORK_TOKEN as seen from the sender of the token:
* In case that the sender of the active token receives the active token back,
  there should be no work to do. This state is reported to all the Nodes.
* In case that the sender of the active token receives Invalidated token,
  it sends again an Active token to its successor

Life of GETWORK_TOKEN as seen from Node


*/
int parmpi_check_inbox(void)
{
	int token_origin;
	int flag = 0;
	struct par_stack_item *oldest_opened;
	int iterate;
	MPI_Status status;
	static int invalidate_tokens = 0;

	do {
		/* Investigate type of received message */
		iterate = false;

		MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
		if (flag != true) {
			break;
		}

		//pr_dbg_mpi("Inbox contains mgs %d\n", status.MPI_TAG);
		switch (status.MPI_TAG) {
		case PARMPI_TAG_NEW_WORK:
			return PARMPI_NEW_WORK;
			break;

		case PARMPI_TAG_NO_WORK:
			MPI_Recv(&void_buff, 1, MPI_INT,
				MPI_ANY_SOURCE, PARMPI_TAG_NO_WORK,
				MPI_COMM_WORLD,	&status);
			return PARMPI_NO_WORK;
			break;

		case PARMPI_TAG_ERROR:
			MPI_Recv(&void_buff, 1, MPI_INT,
				MPI_ANY_SOURCE, PARMPI_TAG_ERROR,
				MPI_COMM_WORLD, &status);
			return PARMPI_ERROR;
			break;

		case PARMPI_TAG_GETWORK_TOKEN_INVALID:
			iterate = true;

			MPI_Recv(&token_origin, 1, MPI_INT,
				MPI_ANY_SOURCE,
				PARMPI_TAG_GETWORK_TOKEN_INVALID,
				MPI_COMM_WORLD,
				&status);

			if (token_origin == parmpi_my_id) {
				/* send valid token again */
				MPI_Send(&token_origin, 1, MPI_INT,
					parmpi_my_successor,
					PARMPI_TAG_GETWORK_TOKEN,
					MPI_COMM_WORLD);
			} else {
				/* send invalid token to successor */
				MPI_Send(&token_origin, 1, MPI_INT,
					parmpi_my_successor,
					PARMPI_TAG_GETWORK_TOKEN_INVALID,
					MPI_COMM_WORLD);
			}
			break;

		case PARMPI_TAG_GETWORK_TOKEN:
			iterate = true;

			MPI_Recv(&token_origin, 1, MPI_INT,
				MPI_ANY_SOURCE, PARMPI_TAG_GETWORK_TOKEN,
				MPI_COMM_WORLD, &status);
			//pr_dbg_mpi("Token contains ID = %d\n", token_origin);

			/* No process has work */
			if (token_origin == parmpi_my_id) {
				/* Send info to all processes */
				pr_dbg_mpi("GETWORK_TOKEN returned\n");
				parmpi_notify_no_work();
				return PARMPI_NO_WORK;

			/* forward token to successor, he can't get work from me */
			} else if ((parmpi_stack_empty == true)
				   && (invalidate_tokens == 0))
			{
				MPI_Send(&token_origin, 1, MPI_INT,
					parmpi_my_successor,
					PARMPI_TAG_GETWORK_TOKEN,
					MPI_COMM_WORLD);

			/* forward invalid token to successor */
			} else if ((parmpi_stack_empty == true)
				   && (invalidate_tokens != 0))
			{
				MPI_Send(&token_origin, 1, MPI_INT,
					parmpi_my_successor,
					PARMPI_TAG_GETWORK_TOKEN_INVALID,
					MPI_COMM_WORLD);
				invalidate_tokens--;

			/* send some job to token_origin */
			} else if (par_get_oldest_opened_state(&oldest_opened) == 0) {

				/* invalidate next tokens from all processes */
				invalidate_tokens = parmpi_instances_cnt - 1;

				parmpi_send_work(oldest_opened, token_origin,
					PARMPI_TAG_NEW_WORK);

			/* Some process wants work but here are no opened states */
			} else {
				return PARMPI_FORCE_CONTINUE;
			}
			break;
		}

	} while (iterate);

	return PARMPI_INBOX_EMPTY;
}

int main(int argc, char *argv[])
{
	int offs = 0;
	int ret;
	int flag;
	int state_id = 0;
	MPI_Status status;
	int all_slaves_have_data = false;
	struct par_state *first_state;

	struct par_state *state;
	struct par_stack_item *stack_item;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &parmpi_my_id);
	MPI_Comm_size(MPI_COMM_WORLD, &parmpi_instances_cnt);
	parmpi_my_successor = (parmpi_my_id + 1) % parmpi_instances_cnt;
	parmpi_check_inbox_cnt = 0;
	parmpi_stack_empty = true;

	/* Only first process will parse arguments */
	if (parmpi_my_id == PARMPI_ROOT) {
		parmpi_starttime = MPI_Wtime();

		ret = parmpi_root_send_parsed_arguments(argc, argv);
		if (ret < 0) {
			pr_dbg_mpi("Sent error status, "
				"this process will be terminated.\n");
			MPI_Finalize();
		}

	} else {
		/* SLAVES will get parsed arguments -- receive bcast */
		ret = parmpi_slaves_receive_parsed_arguments();
		if (ret < 0) {
			pr_dbg_mpi("Received error status, "
				"this process will be terminated.\n");
			MPI_Finalize();
		}
	}

	parmpi_cloud_status = PARMPI_OK;
	parmpi_cloud_status = parmpi_check_gf_size();

	parmpi_initconf_buffer[0] = parmpi_cloud_status;
	MPI_Bcast(parmpi_initconf_buffer, 1, MPI_CHAR, parmpi_my_id, MPI_COMM_WORLD);

	/* Gather status of Global field allocation from all the instances */
	MPI_Barrier(MPI_COMM_WORLD);

	/* Get status from all nodes */
	for (int i = 0; i < parmpi_instances_cnt; i++) {
		if (i == parmpi_my_id)
			continue;

		MPI_Bcast(parmpi_initconf_buffer, 1, MPI_CHAR, i, MPI_COMM_WORLD);
		if (parmpi_initconf_buffer[0] == PARMPI_ERROR)
			parmpi_cloud_status = PARMPI_ERROR;
	}

	if (parmpi_cloud_status == PARMPI_ERROR) {
		pr_dbg_mpi("Error status detected in the cloud, "
			"this process will be terminated.\n");
		MPI_Finalize();
		return -1;
	}

	pr_dbg_mpi("Basic information is consistent in the whole cloud.\n");

	ret = par_stack_init(&stack);
	assert(ret == 0);
	mpi_statemsg_size = par_field_size*sizeof(unsigned char)
		/* global_field_used_rects_types */
		+ PAR_RECT_POSTLAST*sizeof(unsigned int)
		+ 4*sizeof(unsigned int); /* State */
	parmpi_msg_buffer = malloc(mpi_statemsg_size);
	if (!parmpi_msg_buffer) {
		assert(0);
		parmpi_notify_error();
		MPI_Finalize();
		return -1;
	}

	if (parmpi_my_id == PARMPI_ROOT) {
		par_gf_init(global_field);
		par_gf_init(global_field_best);
		par_gf_init(global_field_tmp);

		/* Create first (empty) state and push it to the stack */
		first_state = malloc(sizeof(struct par_state));
		first_state->x = 0;
		first_state->y = 0;
		first_state->rect_type = PAR_RECT_NONE;
		first_state->filled_tiles = 0;
		first_state->flags = PAR_STATE_OPENED;
		ret = par_stack_push(&stack, &first_state->stack_item);
		assert(ret == 0);
		parmpi_stack_empty = false;
	} else {
		 parmi_receive_work_and_push_state(PARMPI_TAG_FIRST_STATE);
		pr_dbg_mpi("Working . . .\n");
	}

	/* ===== MAIN "DFS" LOOP ===== */
	do {
		/* We just generated (and placed onto stack) more States than
		   there is MPI Slaves -- we are able to redistribute the work */
		if ((parmpi_my_id == PARMPI_ROOT)
		    && (state_id >= parmpi_instances_cnt)
		    && (all_slaves_have_data == false))
		{
			for (int dest = 1; dest < parmpi_instances_cnt; dest++) {
				struct par_stack_item *oldest_opened;

				ret = par_get_oldest_opened_state(&oldest_opened);
				if (ret != 0) {
					/* There are more Slaves than Opened states */
					parmpi_notify_no_work();
				}
				parmpi_send_work(oldest_opened, dest,
					PARMPI_TAG_FIRST_STATE);
			}
			all_slaves_have_data = true;
			pr_dbg_mpi("ROOT delegated all work to SLAVES. "
				"Continue in State space expansions.\n");
		}

		ret = par_stack_pop(&stack, &stack_item);
		if (stack_item == NULL) /* Empty stack */
		{
			pr_dbg_mpi("Stack is empty\n");
			/* Try to get work from another node */
			/* MPI TokenMessage format:
			   INT: Token origin
			*/
			pr_dbg_mpi("Asking for new work\n");
			MPI_Send(&parmpi_my_id, 1, MPI_INT,
				parmpi_my_successor, PARMPI_TAG_GETWORK_TOKEN,
				MPI_COMM_WORLD);

			parmpi_stack_empty = true;
			pr_dbg_mpi("Checking inbox <--\n");
			do {
				flag = parmpi_check_inbox();
			}
			while (flag == PARMPI_INBOX_EMPTY);
			pr_dbg_mpi("Inbox checked -->\n");

			if (flag == PARMPI_NO_WORK) {
				pr_dbg_mpi("Done with my work. "
					"There is not anything else to work on.\n");
				break;
			} else if (flag == PARMPI_NEW_WORK) {
				pr_dbg_mpi("Received new work\n");
				parmi_receive_work_and_push_state(PARMPI_TAG_NEW_WORK);
				ret = par_stack_pop(&stack, &stack_item);
				assert(stack_item != NULL);
				pr_dbg_mpi("Working . . .\n");
			} else { /* PARMPI_ERROR */
				MPI_Finalize();
				return -1;
			}
		}

		state = container_of(stack_item, struct par_state, stack_item);

		if (state->flags == PAR_STATE_OPENED) {
			global_field_filled_tiles += par_rect_size[state->rect_type];
			global_field_used_rects_types[state->rect_type]++;
			ret = par_field_place_rect(global_field, state->x, state->y,
				state->rect_type);
			assert(ret == true);

			pr_dbg_state(state, 1);
		}
		else if (state->flags == PAR_STATE_CLOSED) { /* Backtrack */
			pr_dbg("[backtracking this state]\n");
			pr_dbg_state(state, 1);

			global_field_filled_tiles -= par_rect_size[state->rect_type];
			global_field_used_rects_types[state->rect_type]--;
			par_field_remove_rect(global_field, state->x, state->y,
				state->rect_type);
			free(state);

			pr_dbg_field(global_field);

			continue;
		}

		/* Check for end-state? */
		/* Upgrade the best known state */
		if (((global_field_used_rects_types[PAR_RECT_3_3] > 0)
		     && ((global_field_used_rects_types[PAR_RECT_2_4] > 0)
			 || (global_field_used_rects_types[PAR_RECT_4_2] > 0))
		     && ((global_field_used_rects_types[PAR_RECT_1_5] > 0)
			 || (global_field_used_rects_types[PAR_RECT_5_1] > 0)))
		    && (state->filled_tiles > best_state_filled))
		{
			best_state_filled = state->filled_tiles;
			for (int i = 0; i < par_field_width; i++) {
				for (int j = 0; j < par_field_height; j++) {
					global_field_best[i][j] = global_field[i][j];
				}
			}

			if (state->filled_tiles == par_field_size) {
				printf("Full covering was found.\n");
				parmpi_notify_no_work();
				break;
			}
		}

		pr_dbg("[possibly end state]\n");

		/* If this state is not "expanded", push it back onto the stack --
		   after expansion it will be set as CLOSED. It will be used when
		   backtracking from states which are his descendants */
		if (state->flags == PAR_STATE_OPENED) {
			state->flags = PAR_STATE_CLOSED;
			ret = par_stack_push(&stack, stack_item);
			pr_dbg_state(state, 1);
			assert(ret == 0);
			state_id++;
		}

		pr_dbg("[expanding following state]\n");
		pr_dbg_field(global_field);

		/* We just pushed one of the End states -- no possibility to expand it,
		   just backtrack */
		if ((par_field_size - state->filled_tiles) < PAR_RECT_SIZE_SMALLEST) {
			continue;
		}

		/* for_each_tile() */
		for (int i = 0; i < par_field_size; i++) {
			int x = i % par_field_width;
			int y = i / par_field_width;

			if (global_field[x][y] == '\0') {
				/* All possible rectangles for particular tile */
				for (int recttype = PAR_RECT_PREFIRST + 1;
				     recttype < PAR_RECT_POSTLAST; recttype++) {

					struct par_state *state_tmp;
					state_tmp = malloc(sizeof(struct par_state));

					ret = par_field_place_rect(global_field,
						x, y, recttype);
					if (ret == false) {
						free(state_tmp);
						continue;
					}
					par_field_remove_rect(global_field, x, y,
						recttype);


					state_tmp->x = x;
					state_tmp->y = y;
					state_tmp->rect_type = recttype;
					state_tmp->flags = PAR_STATE_OPENED;
					state_tmp->filled_tiles =
						global_field_filled_tiles
						+ par_rect_size[recttype];
					state_tmp->id = state_id++;

					ret = par_stack_push(&stack,
						&state_tmp->stack_item);
					assert(ret == 0);
					pr_dbg_state(state_tmp, 0);
				}
			}
		}

		/* Check for new messages from another processes */
		if (parmpi_check_inbox_cnt >= PARMPI_TEST_NEW_MESSAGES) {
			ret = parmpi_check_inbox();
			switch (ret) {
			case PARMPI_ERROR:
				parmpi_cloud_status = PARMPI_ERROR;
				break;
			case PARMPI_NO_WORK:
				parmpi_cloud_status = PARMPI_FINALIZE;
				break;
			case PARMPI_NEW_WORK: /* This should be impossible */
				parmpi_cloud_status = PARMPI_ERROR;
				parmpi_notify_error();
				break;
			default:
				parmpi_cloud_status = PARMPI_OK;
				break;
			}

			if (parmpi_cloud_status == PARMPI_ERROR) {
				MPI_Finalize();
				return -1;
			} else if (parmpi_cloud_status == PARMPI_FINALIZE) {
				break;
			}

			parmpi_check_inbox_cnt = 0;
		} else {
			parmpi_check_inbox_cnt++;
		}
	} while(true);

	parmpi_clean_inbox();
	MPI_Barrier(MPI_COMM_WORLD);

	/* Reduction */
	do {
		unsigned int reduction_filled = best_state_filled;
		unsigned int reduction_filled_best = best_state_filled;
		unsigned int reduction_best_process = 0;

		pr_dbg_mpi("My best solution filled_tiles: %u\n", best_state_filled);

		/* === SLAVE === */
		if (parmpi_my_id != PARMPI_ROOT) {
			/* Send best state to ROOT */
			MPI_Send(&best_state_filled, 1, MPI_INT,
				PARMPI_ROOT, PARMPI_TAG_SOLUTION_SIZE, MPI_COMM_WORLD);

			/*Wait for root reply */
			MPI_Recv(parmpi_msg_buffer, par_field_size*sizeof(char),
				MPI_PACKED, PARMPI_ROOT, MPI_ANY_TAG, MPI_COMM_WORLD,
				&status);
			if (status.MPI_TAG == PARMPI_TAG_REDUCTION_SELECT) {
				/* Send my solution to root */
				offs = 0;
				for (int i = 0; i < par_field_width; i++) {
					MPI_Pack(global_field_best[i], par_field_height,
						MPI_CHAR, parmpi_msg_buffer,
						mpi_statemsg_size, &offs,
						MPI_COMM_WORLD);
				}

				MPI_Send(parmpi_msg_buffer, offs, MPI_PACKED,
					PARMPI_ROOT, PARMPI_TAG_SOLUTION,
					MPI_COMM_WORLD);
			} else {
				break;
			}
		/* === ROOT === */
		} else {
			/* get best states from all SLAVEs */
			for (int node = 1; node < parmpi_instances_cnt; node++) {
				MPI_Recv(&reduction_filled, 1,
					MPI_INT, node, PARMPI_TAG_SOLUTION_SIZE,
					MPI_COMM_WORLD, &status);

				pr_dbg_mpi("root: reduction_filled = %u\n",
					reduction_filled);

				if (reduction_filled > reduction_filled_best) {
					reduction_filled_best = reduction_filled;
					reduction_best_process = node;
				}
			}

			/* Send reply to all slaves */
			for (int node = 1; node < parmpi_instances_cnt; node++) {
				if (node == reduction_best_process) {
					MPI_Send(&void_buff, 0, MPI_PACKED,
						node, PARMPI_TAG_REDUCTION_SELECT,
						MPI_COMM_WORLD);
				} else {
					MPI_Send(&void_buff, 0, MPI_PACKED,
						node, PARMPI_TAG_REDUCTION_STOP,
						MPI_COMM_WORLD);
				}
			}

			/* Wait for best filled field */
			if (reduction_best_process != PARMPI_ROOT) {
				MPI_Recv(parmpi_msg_buffer, mpi_statemsg_size,
					MPI_PACKED, reduction_best_process,
					PARMPI_TAG_SOLUTION, MPI_COMM_WORLD, &status);

				best_state_filled = reduction_filled_best;

				offs = 0;
				for (int i = 0; i < par_field_width; i++) {
					MPI_Unpack(parmpi_msg_buffer,
						mpi_statemsg_size, &offs,
						global_field_best[i],
						par_field_height,
						MPI_CHAR, MPI_COMM_WORLD);
				}
			}

			parmpi_endtime = MPI_Wtime();

			pr_dbg_mpi("reduction_best_process = %u\n",
				reduction_best_process);
			/* Print best state  */
			if (best_state_filled) {
				printf("Best found solution (MPI process ID %d):\n",
					reduction_best_process);
				par_field_print(global_field_best);
				printf("Unfilled tiles = %d\n",
					(par_field_size - best_state_filled));
			} else {
				printf("Solution meeting the correctness condition"
					"(of using each rectangle at least once) wasn't found.\n");
			}
			printf("Computation took %f seconds\n",
				parmpi_endtime - parmpi_starttime);
		}
	} while(false);

	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Finalize();

	/* free memory */
	while (true) {
		ret = par_stack_pop(&stack, &stack_item);
		if (stack_item == NULL) /* Empty stack */
			break;

		state = container_of(stack_item, struct par_state, stack_item);
		free(state);
	}

	free(parmpi_msg_buffer);
	par_gf_dealloc(global_field);
	par_gf_dealloc(global_field_best);
	par_gf_dealloc(global_field_tmp);

	return 0;
}
