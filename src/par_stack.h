#ifndef _PAR_STACK_H_
#define _PAR_STACK_H_

#include <stddef.h>

#if 1
#ifdef __GNUC__
#define member_type(type, member) __typeof__ (((type *)0)->member)
#else
#define member_type(type, member) void
#endif

#define container_of(ptr, type, member) ((type *)( \
    (char *)(member_type(type, member) *){ ptr } - offsetof(type, member)))
#endif

#if 0
#define container_of(ptr, type, member) ({ \
	const typeof( ((type *)0)->member ) *__mptr = (ptr); \
	(type *)( (char *)__mptr - offsetof(type,member) );})
#endif
/* -------------------------------------------------------------------------- */

#define PAR_STACK_ERR_OK	 0 /* Everything is ok -- do no change;
				      our assert()s expect this value */
#define PAR_STACK_ERR_NOSTACK 	-1
#define PAR_STACK_ERR_NOITEM 	-2


struct par_stack_item {
	struct par_stack_item *parent;	/* Pointer to the item already in stack */
};

struct par_stack {
	struct par_stack_item *sp; 	  /* Stack pointer */
	struct par_stack_item first_item; /* First element used to determine
					if the stack is empty (i.e. sp == &first_item*/
};

/* -------------------------------------------------------------------------- */

int par_stack_init(struct par_stack *stack);
int par_stack_pop(struct par_stack *stack, struct par_stack_item **item);
int par_stack_push(struct par_stack *stack, struct par_stack_item *item);
int par_stack_get_predecessor(struct par_stack *stack, const struct par_stack_item *item,
			      struct par_stack_item **predecessor);
int par_stack_remove_predecessor(struct par_stack *stack,
				 struct par_stack_item *item);
int par_stack_get_top(struct par_stack *stack, struct par_stack_item **item);
#endif /* _PAR_STACK_H_ */
