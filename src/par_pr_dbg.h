#ifndef _PAR_PR_DBG_H_
#define _PAR_PR_DBG_H_

#include "stdio.h"

#define DEBUG_PREFIX 		"" //"par_sem: "
#define DEBUG_PREFIX_MPI	"MPI"


#ifdef DEBUG_PARSEM
	#define pr_dbg(...) \
	fprintf(stderr, DEBUG_PREFIX __VA_ARGS__)
#else
	#define pr_dbg(...)
#endif


#ifdef DEBUG_PARSEM_MPI
	#define pr_dbg_mpi(FormatLiteral, ...) \
	fprintf(stderr, DEBUG_PREFIX_MPI "(%d): " FormatLiteral, parmpi_my_id, ##__VA_ARGS__)
#else
	#define pr_dbg_mpi(...)
#endif

#endif /* _PAR_PR_DBG_H_ */
