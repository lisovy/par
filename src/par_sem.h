#ifndef _PAR_SEM_H_
#define _PAR_SEM_H_

#include "par_stack.h"


enum par_rect { PAR_RECT_NONE,
	PAR_RECT_PREFIRST, /* used when iterating over the possible rectangles */
	PAR_RECT_3_3, /* First nr. in name is X-axis, second Y-axis */
	PAR_RECT_2_4,
	PAR_RECT_1_5,
	PAR_RECT_4_2,
	PAR_RECT_5_1,
	PAR_RECT_POSTLAST /* Ditto PAR_RECT_PREFIRST*/ };

enum { 	PAR_STATE_OPENED,
	PAR_STATE_CLOSED };

/* Lookup tables describing width, height and tiles count of particular rectangle */
unsigned char par_rect_width[] =
{	[PAR_RECT_NONE] = 0,
	[PAR_RECT_3_3] = 3,
	[PAR_RECT_2_4] = 2,
	[PAR_RECT_1_5] = 1,
	[PAR_RECT_4_2] = 4,
	[PAR_RECT_5_1] = 5
};

unsigned char par_rect_height[] =
{	[PAR_RECT_NONE] = 0,
	[PAR_RECT_3_3] = 3,
	[PAR_RECT_2_4] = 4,
	[PAR_RECT_1_5] = 5,
	[PAR_RECT_4_2] = 2,
	[PAR_RECT_5_1] = 1
};

unsigned char par_rect_size[] =
{	[PAR_RECT_NONE] = 0,
	[PAR_RECT_3_3] = 9,
	[PAR_RECT_2_4] = 8,
	[PAR_RECT_1_5] = 5,
	[PAR_RECT_4_2] = 8,
	[PAR_RECT_5_1] = 5
};
#define PAR_RECT_SIZE_SMALLEST		5 	/* Size of the smallest rectangle */
#define PAR_FIELD_MIN			9+8+5 	/* Minimal size of the field to place
						   each rectangle at least once */

unsigned char par_rect_type[] =
{	[PAR_RECT_NONE] = 0,
	[PAR_RECT_3_3] = 1,
	[PAR_RECT_2_4] = (1 << 1),
	[PAR_RECT_1_5] = (1 << 2),
	[PAR_RECT_4_2] = (1 << 1),
	[PAR_RECT_5_1] = (1 << 2)
};



/* One possible state of placing rectangles into the field */
struct par_state {
	/* The only change of this state compared to its predecessor */
	unsigned int x;
	unsigned int y;
	enum par_rect rect_type;

	/* Nr. of tiles filled in global field by this state */
	unsigned int filled_tiles;

	/* Unique state identifier */
	unsigned int id; // FIXME is enough or make it long long ?

	/* Flags of the state -- e.g. CLOSED, OPENED */
	unsigned int flags;

	/* Used for pushing/poping to/from the stack */
	struct par_stack_item stack_item;
};

#endif /* _PAR_SEM_H_ */
